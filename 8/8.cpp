#include <stdio.h>
#include <iostream>
#include <deque>
#include <string.h>
#include <assert.h>
#define N 100
using namespace std;
char M[N+1][N+1][N+1];

int isB(int a,int b, int c){
    if( M[a][b][c] == 'B')
        return 1;
    return 0;
}
bool canStepin(int a, int b, int c){
    if(!(a>N-1 || b>N-1 || c>N-1 || a<0 || b<0 || c<0 ||  M[a][b][c] == '#'))
        return true;
    return false;
}
typedef struct{
    int x,y,z,step,B;
}locS;


void assignlocs(locS *L, int a, int b, int c, int B, int step)
{
    L->x=a;
    L->y=b;
    L->z=c;
    L->step=step;
    L->B=B;
}
bool findExit(int a, int b, int c, int a1, int b1, int c1)
{
    if(a == a1 && b == b1 && c== c1)
        return true;
    return false;
}
bool findDestination(int a,int b, int c, locS *P,int n)
{

    if((a<n-1 && findExit(a+1,b,c, P->x, P->y, P->z))||(a>0 && findExit(a-1,b,c, P->x, P->y, P->z))||(b<n-1 && findExit(a,b+1,c,  P->x, P->y, P->z))||
        (b>0 && findExit(a,b-1,c,  P->x, P->y, P->z))||(c<n-1 && findExit(a,b,c+1, P->x, P->y, P->z))||(c>0 && findExit(a,b,c-1,  P->x, P->y, P->z)))
        return true;
    return false;
}

int main()
{
    int caseNum;
    cin>>caseNum;
    assert(1<=caseNum && caseNum<=10);
   
    memset(M, '\0', sizeof(M));
    while(caseNum--)
    {
        int  x, y, z, stepAns,BAns,n;
        locS E,S;
        cin>>n;;
        BAns = 0;
        stepAns = 0;
        bool Arrival = false;
        assert(2<=n && n<= 100);
        assignlocs(&E, 0, 0, 0, 0 ,0);
        assignlocs(&S, 0, 0, 0, 0, 0);
        x=0;y=0;z=0;
        int tempS=0, tempB=0;
        deque <locS> Q;
        char cc;
        for(int i = 0; i != n*n*n; ++i)
        {
            cin>>cc;
            x = i/(n*n);
            y = (i%(n*n))/n;
            z = i%n;
            M[x][y][z]=cc;
            if( cc == 'E'){
                assignlocs(&E, x, y, z, 0, 0);
            }
            else if( cc == 'S'){
                assignlocs(&S, x, y, z, 0, 0);
            }
        }

        Q.push_back(S);
        //Q1.push(make_pair(0,0));
        while(!Q.empty()){
            if(stepAns!=0  && Q.front().step>=stepAns){
                Q.clear();
                continue;
            }
            locS tmp, next;
            int a,b,c;
            assignlocs(&tmp, Q.front().x, Q.front().y,Q.front().z,Q.front().B,Q.front().step);
            tempB=tmp.B;
            tempS=tmp.step;
            a = tmp.x;
            b = tmp.y;
            c = tmp.z;
            Q.pop_front();
           if(findDestination(a,b,c,&E,n)){
                if(tempB > BAns) BAns = tempB;
                stepAns= tempS+1;
                Arrival = true;
            }
            if (Arrival == false) {
                if(canStepin(a+1,b,c)) { 
                    assignlocs(&next, a+1, b, c, tempB+isB(a+1,b,c), tempS+1);
                    Q.push_back(next);
                }
                if(canStepin(a-1,b,c)) {
                    assignlocs(&next, a-1, b, c, tempB+isB(a-1,b,c), tempS+1);
                    Q.push_back(next);
                }
                if(canStepin(a,b+1,c)){
                    assignlocs(&next, a, b+1, c, tempB+isB(a,b+1,c), tempS+1);
                     Q.push_back(next);
                }
                if(canStepin(a,b-1,c)){
                    assignlocs(&next, a, b-1, c, tempB+isB(a,b-1,c), tempS+1);
                     Q.push_back(next);
                }
                if(canStepin(a,b,c+1)){
                    assignlocs(&next, a, b, c+1, tempB+isB(a,b,c+1), tempS+1);
                     Q.push_back(next);;
                }
                if(canStepin(a,b,c-1)){
                    assignlocs(&next, a, b, c-1, tempB+isB(a,b,c-1), tempS+1);
                     Q.push_back(next);
                }
            }
            M[a][b][c]= '#';
        }
        if(Arrival==false)
            printf("Fail OAQ\n");
        else
            printf("%d %d\n",stepAns,BAns);
    }
    return 0;

}
