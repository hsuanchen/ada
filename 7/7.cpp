# include <stdio.h>
# include <stdlib.h>
#include <string.h>
#define SET_SIZE 10
int setOfCoins[8] = {1,5,10,20,100,200,1000,2000};
int setOfCoinsOri[10]={1,5,10,20,50,100,200,500,1000,2000};
int coins[SET_SIZE] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//{1,5,10,20,50,100,200,500,1000,2000};
/*bool change(int amount, int type){
 int  k = amount/setOfCoins[type];
  
}*/
int main(void)
{
    //Set of coins
       // User input change amount
      int changeAmount,caseNum, *ans;
      //Saves the coin solution
      int tmp, total,sum,rest;

      int count;
       // Variable changes to 1 if no solution was found else it's 0
      
       int coin_50,coin_500;
       scanf("%d",&caseNum);
       ans = new int [caseNum];
  for(int z = 0; z != caseNum; ++z){
      //printf("Input change amount, and I will give you the least amount of coins\n");
        scanf("%d", &changeAmount);
        total=0;sum=0;
    for(int i = 0; i != 10; ++i)
      scanf("%d",&coins[i]);
    coin_50=coins[5];
    coin_500=coins[7];
      
    for(int i = 0; i != 10; ++i){
      sum+=coins[i]*setOfCoinsOri[i];
     // total+=coins[i];
     }
     rest=sum-changeAmount;
     // printf("rest: %d\n", rest);
    if(coins[4]%2==0&&coins[7]%2==0){
      coins[5]+=coins[4]/2;
      coins[7]+=coins[8]/2;
    //  total=total-(coin_50+coin_500)/2;
      coins[4]=0;
      coins[8]=0;
    }
    else if(coins[4]%2==1&&coins[7]%2==1)
    {
      rest-=550;
      coins[5]+=coins[4]/2;
      coins[7]+=coins[8]/2;
    //  total=total-(coin_50+coin_500)/2;
      coins[4]=0;
      coins[8]=0;
    }
    else if(coins[4]%2==0&&coins[7]%2==1)
    {
      coins[5]+=coins[4]/2;
      coins[7]+=coins[8]/2;
    //  total=total-(coin_50+coin_500)/2;
      coins[4]=0;
      coins[8]=0;
      rest-=500;
    }
    else{
     coins[5]+=coins[4]/2;
      coins[7]+=coins[8]/2;
   //   total=total-(coin_50+coin_500)/2;
      coins[4]=0;
      coins[8]=0;
      rest-=50;
    }
    int newCoins[8]={coins[0],coins[1],coins[2],coins[3],coins[5],coins[6],coins[7],coins[9]};  
     count=7;
     while(rest > 0&&count>=0)
    {
            tmp=newCoins[count];
             while( setOfCoins[count] <= rest && --tmp>=0)
               {
                   //The new change amount
                  rest = rest - setOfCoins[count] ;
               //     printf("rest: %d\n", rest);
                   // Remember coins used
                  //solution[count] +=1;
                    --newCoins[count];
               }
             count = (count-1); // This makes sure that the count index never goes beyond SET_SIZE
     }

     if(rest==0)
      {
      for(int i = 0; i != 8; ++i)
          total+=newCoins[i];
      
        if(newCoins[6]>0)
        {
         if(newCoins[6]>=coin_500/2)
           total+=coin_500/2;
          else
         total+=newCoins[6];
        }
         if(newCoins[4]>0)
         {
         if(newCoins[4]>=coin_50/2)
          total+=coin_50/2;
         else
           total+=newCoins[4];
        }
      }
      else
        total=-1;
      ans[z]=total;
    }
    for(int z = 0; z != caseNum; ++z)
    printf("%d\n",ans[z]);
    
    return 0;
    //system("pause"); //This is only for windows operating system otherwise comment it out.
}
