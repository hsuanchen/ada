#include <cstdio>
#include <cstring>
#include <algorithm>
#include <stdlib.h>

int main(){
	int N, P,p,caseNum,*ans,max;
	scanf("%d",&caseNum);
	ans= new int [caseNum];
	for(int z = 0; z != caseNum; ++z)
	{
		scanf("%d%d",&N,&P);
		int *location;
		location = new int [N+1];
		for(int i = 0; i != N; ++i)
			scanf("%d",&location[i+1]);
		std::sort(location+1,location+N+1);
		int k=location[N]-location[1];
		max=1000000000;
		int curP,OK,start=0, end=k,state, centre;
		while(end>start){
			curP=1;
			p=P;
			centre=0;
			for(int i = 2; i != N+1; ++i)
			{
				if(location[i]>location[curP]+k){
					if(p==0)
						OK=0;
					else if(centre==0){
					centre=1;
					curP=i-1;
					--i;	
					}
					else{
					--p;
					centre=0;
					curP=i;
					if(i==N&&p==0)
						OK=0;
					}
				}
				//printf("location:%d curP:%d\n",location[i],curP);
				if(centre==0&&p==0) OK=0;
				if(i==N&&p>0)
						OK=1;
			}
		
		if(OK==1){
			end=k;
			if(k<max) max=k;
		}
		else
			start=k+1;
		//printf("k: %d max: %d\n",k,max);
		//if(prevS==start&&prevE==end)
		//	break;
		k=(start+end)/2;
		//prevS=start;
	//	prevE=end;
		//printf("start:%d  end:%d",start,end);
		}
		ans[z]=max;
	}
	for(int z = 0; z != caseNum; ++z)
		printf("%d\n",ans[z]);
	delete [] ans;
		return 0;
}