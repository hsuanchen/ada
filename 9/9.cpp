
#include <iostream>
#include <list>
#include <queue>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <memory>
using namespace std;

bool inline bitindex(int a, int b)
{
   if( a & (1<<b) )
       return true;
    else
        return false;
}
bool inline dummyE(int w){
    if (w==0)
        return true;
    else
        return false;
}
class Graph
{
private:
    int V;
    int E;
    list< pair<int,int> > *adj;
    list< pair<int,int> > *rec;
public:
    Graph(int V)
    {
        this->V = V;
        E=0;
        adj = new list< pair<int,int> >[V+1];
        rec = new list< pair<int,int> >[V+1];
    }
    ~Graph()
    {
  //      cleanBackup();
  //      cleanAdj();
        
    }
    void addEdge(int v, int u,int w);
    void trim(int index);
    void recover();
    void cleanBackup();
  //  void cleanAdj();
    void BFS(int s, bool visited[]);
    Graph getTranspose();
    bool isConnected();
    void printAdjlist();
    int OR();
    bool MST();
};

/*
 * Add Edge to connect v and w
 */
void Graph::addEdge(int v, int u, int w)
{
    adj[v].push_back(make_pair(u,w));
    adj[u].push_back(make_pair(v,w));
    E=E+2;
}

void Graph::trim(int index){
    list< pair<int,int> >::iterator it;
    
    for(int i = 0 ; i != V; ++i)
    {
        for(it = adj[i].begin(); it != adj[i].end();)
        {
            if( it->second & (1<<index) )
            {
                rec[i].push_back(make_pair(it->first,it->second));
                it=adj[i].erase(it);
                --E;
            }
            else
                ++it;
        }
    }
}

void Graph::recover(){
    list< pair<int,int> >::iterator it;
    
    for(int i = 0 ; i != V; ++i)
    {
        for(it = rec[i].begin(); it != rec[i].end(); ++it)
        {
            adj[i].push_back(make_pair(it->first,it->second));
            ++E;
            
        }
        
    }
    
}

void Graph::cleanBackup(){
    list< pair<int,int> >::iterator it;
    
    for(int i = 0 ; i != V; ++i)
    {
        rec[i].clear();
    }

}
/*
void Graph::cleanAdj(){
    list< pair<int,int> >::iterator it;
    
    for(int i = 0 ; i != V; ++i)
    {
        for(it = adj[i].begin(); it != adj[i].end(); ++it)
        {
            adj[i].erase(it);
        }
    }
    
}*/
/*
 *  A recursive function to print BgFS starting from s
 */
void Graph::BFS(int s, bool visited[])
{
    list<int> q;
    list< pair<int,int> >::iterator i;
    visited[s] = true;
    q.push_back(s);
    while (!q.empty())
    {
        s = q.front();
        q.pop_front();
        for(i = adj[s].begin(); i != adj[s].end(); ++i)
        {
            if(!visited[i->first])
            {
                visited[i->first] = true;
                q.push_back(i->first);
            }
        }
    }
}
/*
 * Function that returns reverse (or transpose) of this graph
 */
Graph Graph::getTranspose()
{
    Graph g(V);
    for (int v = 0; v < V; v++)
    {
        list< pair<int,int> >::iterator i;
        for(i = adj[v].begin(); i != adj[v].end(); ++i)
        {
            g.adj[i->first].push_back(make_pair(v,0));
        }
    }
    return g;
}
/*
 * Check if Graph is Connected
 */

bool Graph::isConnected()
{
    bool visited[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;
    BFS(0, visited);
    for (int i = 0; i < V; i++)
        if (visited[i] == false)
            return false;
    Graph gr = getTranspose();
    for(int i = 0; i < V; i++)
        visited[i] = false;
    gr.BFS(0, visited);
    for (int i = 0; i < V; i++)
        if (visited[i] == false)
            return false;
    return true;
}

void Graph::printAdjlist(){
    for(int j = 0; j != V; ++j){
    list< pair<int,int> >::iterator i;
    for(i = adj[j].begin(); i != adj[j].end(); ++i)
    {
        printf("(%d,%d):%d\n",j,i->first,i->second);
    }
    }
}

int Graph::OR(){
    int ans=0;
    list< pair<int,int> >::iterator i;
    for(int v = 0; v != V; ++v){
        for(i = adj[v].begin(); i != adj[v].end(); ++i)
        {
            ans = ans|i->second;
        }
    }
    return ans;
}

bool Graph::MST(){
    if(E == 2*(V-1))
        return true;
    else
        return false;
}

int main()
{
    int V,edge,a,b,c,maxe=0;
    int t;
    scanf("%d",&t);
    assert(1<=t && t<=10);
    while(t--){
        scanf("%d %d",&V,&edge);
        assert(2<=V && V <= 100000);
        assert(V-1 <= edge && edge <= 200000);
        Graph g(V);
        while(edge--)
        {
            scanf("%d %d %d",&a,&b,&c);
            assert(1 <= a && 1 <= b && a <= V && b<= V);
            assert(0 <= c && c<= 1000000000);
            g.addEdge(a-1,b-1,c);
            if(c>maxe) {
                maxe=c;
            }
        }
        assert(g.isConnected());
        int index = 0;
        while(maxe >= pow(2.0,index) ){
            ++index;
        }
        --index;
  // cout<<"index:"<<index<<endl;
    //     g.printAdjlist();
        for(int i = index; i >= 0; --i)
        {
            g.trim(i);
            if(!g.isConnected())
                g.recover();
            g.cleanBackup();
      //      cout<<"trim index:"<<i<<endl;;
        //    g.printAdjlist();
            if(g.MST())
                i=-1;
        }
     //   if(modify++==0)printf("\n");
        printf("%d\n",g.OR());
    }
    return 0;

}
