#include <stdio.h>
#include <iostream>
//using namespace std;
typedef unsigned long long LL;
unsigned long long g[65];
int hash[10008] = {};
int mx, special, n;
void dfs(LL state, int ch, LL mark, LL noch) {
    //printf("%lld %d\n", state.v, ch);
    if(state == 0) {
        if(mx < ch)
            mx = ch;
        if(mark == (1LL<<n)-1)
            special++;
        return;
    }
    LL lowbit = state&(-state);
    int idx = hash[lowbit%10007];
    //printf("lowbit %lld idx %d\n", lowbit, idx);
    LL tmp = state;
    tmp = tmp ^ (tmp&g[idx]);
    dfs(tmp, ch+1, mark|g[idx], noch);
    tmp = state ^ lowbit;
    dfs(tmp, ch, mark, noch|lowbit);
}
int main() {
    int testcase;
    int m, x, y;
    int i, j, k;
    for(i = 0; i < 63; i++) {
        if(hash[(1LL<<i)%10007])
            puts("ERR");
        hash[(1LL<<i)%10007] = i;
    }
    scanf("%d", &testcase);
    while(testcase--) {
        scanf("%d %d", &n, &m);
        for(i = 0; i < n; i++)
            g[i] = 1LL<<i;
        while(m--) {
            scanf("%d %d", &x, &y);
            g[x] |= 1LL<<y;
            g[y] |= 1LL<<x;
        }
        LL state;
        state = (1LL<<n)-1;
        mx = 1, special = 0;
        dfs(state, 0, 0, 0);
        printf("%d\n%", mx);
    }
    return 0;
}