#include <cstdio>
#include <cstring>
#include <bitset>
#include <iostream>
int N, resN, resMax;
using namespace std;
bitset<100> Adj[100], Adja[100], All;
void go(int k, int in, bitset<100> cov) {
    if(resMax != 0 && in+N-cov.count() < resMax){
        return;
    }
    for (;; k++) {
       
        if ((cov | Adja[k]) != All) return;
        if (cov==All) {
            resMax = resMax>in?resMax:in;
            return;
        }
        if (k >= N)
            return;
        if (!cov[k])
        {
            go(k+1, in+1, cov|Adj[k]);
        }
    }
}

int main() {
    int T, M, x, y;
    scanf("%d", &T);
    while (T-- > 0 && scanf("%d %d", &N, &M) == 2 && N > 0) {
        memset(Adj, 0, sizeof(Adj));
        while (M-- > 0 && scanf("%d %d", &x, &y) == 2) {
            Adj[x].set(y);
            Adj[y].set(x);
        }
        for (int i = 0; i < N; i++) Adj[i].set(i);
        Adja[N] = 0;
        for (int i = N-1; i >= 0; i--) Adja[i] = Adj[i] | Adja[i+1];

        for (int i = 0; i < N; i++) All.set(i);
        resMax = 0;
        go(0, 0, 0);
        printf("%d\n",resMax);


    }
}