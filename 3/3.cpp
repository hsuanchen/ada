#include <vector>
#include <stdio.h>
typedef long long int ll;
int main()
{
	int caseNum;
	scanf("%d", &caseNum);
	long int **result;
	result = new long int *[caseNum];
	long int n; //num of players
	long int m; //num of gold blocks
	long int q; //num of events
	ll v; //gold gain in a event
	long int le=0;
	long int ri=0;
	ll temp = 0;
	long int doneCount;
	std::vector<ll> goal;
	std::vector<ll> goldblock;

	for(int i = 0; i != caseNum; ++i)
	{
		doneCount = 0;
		scanf("%ld%ld%ld",&n,&m,&q);
		long int k= n+1;
		result[i] = new long int[k];
		result[i][0]=n;
		//set goal for each player
		for (long int j = 0; j != n; ++j)
		{

			scanf("%lld", &temp);
			goal.push_back(temp);
			result[i][j+1] = -1;
		}
		
		//set gold blocks
		for (long int j = 0; j != m; ++j)
		{
			scanf("%lld",&temp);
			--temp;
			goldblock.push_back(temp);
		}
		
		//event handling
		for (long int j = 1; j != q+1; ++j)
		{	
			scanf("%ld%ld%lld", &le, &ri, &v);
			if (doneCount == n) continue;
			--le;
			--ri;
			for(long int start = le; start != ri+1; ++start)
			{
				if(result[i][goldblock[start]+1] != -1) continue;
				goal[goldblock[start]] -= v;
				if (goal[goldblock[start]] <= 0)
				{
					++doneCount;
					result[i][goldblock[start]+1] = j;
				}
			}
		}
		goal.clear();
		goldblock.clear();
	}
	for(long int i = 0 ;i != caseNum; ++i)
	{
		for(long int j = 0; j != result[i][0]; ++j)
		{
			printf("%ld",result[i][j+1]);
			if(j != result[i][0]-1) printf(" ");
		}
		printf("\n");
	}

	for(long int i = 0; i < caseNum; i++)
        delete [] result[i];
    delete [] result;

}