#include <vector>
#include <stdio.h>
typedef long long int ll;
int main()
{
	int caseNum;
	scanf("%d", &caseNum);
	long int n; //num of players
	long int m; //num of gold blocks
	long int q; //num of events
	ll v; //gold gain in a event
	long int le=0;
	long int ri=0;
	ll temp = 0;
	std::vector<ll> goal;
	std::vector<ll> goldblock;
	
	std::vector<long int> turn;

//	scanf("%ld%ld%ld",&n,&m,&q);
//	printf("%ld %ld %ld\n",n, m, q );
//	scanf("%lld %ld %ld", &v, &l, &r);
	
	for(int i = 0; i != caseNum; ++i)
	{
		scanf("%ld%ld%ld",&n,&m,&q);

//		printf("n %ld m %ld q %ld\n",n,m,q);
		

		//set goal for each player
		for (long int j = 0; j != n; ++j)
		{
			turn.push_back(-1);
		}

		for (long int j = 0; j != n; ++j)
		{
			scanf("%lld", &temp);
			goal.push_back(temp);
		}
		
		//set gold blocks
		for (long int j = 0; j != m; ++j)
		{
			scanf("%lld",&temp);
			--temp;
			goldblock.push_back(temp);
		}
/*		for (long int j = 0; j != m; ++j)
		{
			printf("block %ld belongs to %lld\n", j, goldblock[j] );
		}*/
		//print vecttor now
		
		//event handling
		for (long int j = 1; j != q+1; ++j)
		{
			
			scanf("%ld%ld%lld", &le, &ri, &v);
			--le;
			--ri;
			for(long int start = le; start != ri+1; ++start)
			{
				if (turn[goldblock[start]] == -1)
				{
					goal[goldblock[start]] -= v;
//					printf("goldblock %lld %lld\n",goldblock[start],goal[goldblock[start]] );

				}
			}
			

			for(long int z = 0; z != n; ++z)
			{
				if (goal[z] <= 0 && turn[z] == -1)
				{
					turn[z] = j;
				}

			}
			
		
		}	

		for(long int j = 0; j != n; ++j)
		{
			printf("%ld ",turn[j]);
		}
		printf("\n");
		turn.clear();
		goal.clear();
		goldblock.clear();

	}
	
}