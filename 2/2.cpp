#include <stdio.h>
#include <vector>
#include <utility>
typedef long long lld;
int main()
{
   int caseNum;
   lld * ans;
  
   
   scanf("%d",&caseNum);
   
   ans = new lld[caseNum];

   for(int i=0;i<caseNum;++i)
   {
      ans[i]=0;
   }
   std::vector<std::pair<lld, lld> > v;
   for(int j=0; j<caseNum; ++j)
   {
      
         int boxNum;
         scanf("%d",&boxNum);
         lld w;
         lld h;

         for(int i = 0; i < boxNum; ++i)
         {
         scanf("%lld %lld",&w,&h);
         //put w and h in pair in a vector
         v.push_back(std::make_pair(w,h));
         }
         std::vector<std::pair<lld, lld> >::iterator iter1= v.begin(); 
         std::vector<std::pair<lld, lld> >::iterator iter2;
         for (;iter1!=v.end();++iter1)
         {
            std::pair<lld,lld> current =  std::make_pair(iter1->first, iter1->second);
            for(iter2=v.begin();iter2!=v.end();++iter2)
            {
               if (iter1!=iter2)
               {
                  //printf("iter2 1st:%lld 2nd:%lld\n",iter2->first,iter2->second);
                  if((current.first >= iter2->first && current.second >= iter2->second)|| (current.first >= iter2->second && current.second >= iter2->first) )
                     
                  ++ans[j];
                  
               }
            }  
         }
       v.clear();
   }
    
 
         
  
   for (int i=0; i<caseNum; ++i) printf("%lld\n",ans[i]);
   delete [] ans;
   return 0;
}
