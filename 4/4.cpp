#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <vector>
#define MAX_SIZE 2000

std::string L[MAX_SIZE][MAX_SIZE];
char X[MAX_SIZE], Y[MAX_SIZE];
std::vector<std::string> result;
std::string minS(std::string a, std::string b)
{
    return (a < b)? a : b;
}
std::string bigS(std::string a, std::string b)
{
    return (a.size() > b.size())? a : b;
}
/* Returns length of LCS for X[0..m-1], Y[0..n-1] */
void LCS( char *X, char *Y)
{
    int m = strlen(X);
    int n = strlen(Y);
    for(int i = 1; i !=m+1; ++i)
    {
        for(int j = 1; j != n+1; ++j)
        {
            if(X[i-1] == Y[j-1])
            {
                L[i][j] = L[i-1][j-1] + X[i-1];
            }
            else if(L[i-1][j].size()==L[i][j-1].size())
            {
                 L[i][j] = minS(L[i-1][j], L[i][j-1]);
            }
            else 
            {
              L[i][j] = bigS(L[i-1][j], L[i][j-1]);
            }
        }
    }
    result.push_back (L[m][n]);

}


/* Driver program to test above function */
int main()
{
    int caseNum;
  //  memset(prev, 0, sizeof(prev));
   // memset(L, "", sizeof(L));
    scanf("%d\n", &caseNum);
   for(int c = 0; c != caseNum; ++c)
    {
    fgets(X, 2000, stdin);
    fgets(Y, 2000, stdin);

    LCS(X, Y);
    

    }
    for(std::vector<std::string>::iterator it = result.begin(); it != result.end(); ++it)
        std::cout<<*it;
    
    return 0;
}