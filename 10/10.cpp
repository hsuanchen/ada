//
//  main.cpp
//  9
//
//  Created by Lin Hsuanchen on 2014/12/31.
//  Copyright (c) 2014年 Lin Hsuanchen. All rights reserved.
//

#include <iostream>
#include <stack>
#include <vector>
#include <stdio.h>
#include <utility>
using namespace std;
typedef struct node{
    int iq;
    node* call;
    int index;
    int ans;
}_node;
int main(int argc, const char * argv[]) {
    
    int t;
    scanf("%d", &t);
    while(t--){
    int persons;
    scanf("%d", &persons);
    vector<node> p;
    int max = 0, indexofMax = -1;
    _node n = {0,NULL,0,0};
    p.push_back(n);
    for(int i = 1; i <= persons; ++i)
    {
        int tmp;
        scanf("%d", &tmp);
        _node t;
        t.iq = tmp;
        t.call = NULL;
        t.ans = 0;
        t.index = i;
        p.push_back(t);
        if(tmp > max)
        {
            max = tmp;
            indexofMax = i;
        }
    }
    p[indexofMax].ans = 0;
    int count = persons-1;
    int i;
    if(indexofMax == persons)
        i = 1;
    else
        i = indexofMax+1;
    for(; count != 0; ++i)
    {
        --count;
        if( i == persons+1)
            i = 1;
        if(p[i].iq == max){
            p[i].ans = 0;
            continue;
        }
        int index;
        if( i == 1)
             index = persons;
        else
             index = i - 1;
        
        int tmp = p[index].iq;
        while(p[i].iq >= tmp)
        {
            index = p[index].call->index;
            tmp = p[index].iq;
        }
        p[i].ans = index;
        p[i].call = &p[index];
    }
    for(int i = 1; i < persons; ++i)
    {
        printf("%d ", p[i].ans);
    }
        printf("%d\n",p[persons].ans);
    }
    /*
    int t;
    scanf("%d", &t);
    //while(t--){
    int persons;
    scanf("%d", &persons);
    vector<int> iq(persons+1, 0);
    vector<int> ans(persons+1, 0);
    int tmp;
    int max = 0;
    int indexOfMax = 0;
    stack< pair<int,int> > s;
    for(int i = 1 ; i <= persons; ++i)
    {
        scanf("%d", &tmp);
        iq[i] = tmp;
        if(tmp > max){
            max = tmp;
            indexOfMax = i;
        }
    }
    ans[indexOfMax] = 0;
    s.push(make_pair(indexOfMax,max));
    int cycle = persons-1;
    int i = (indexOfMax == persons? 1:indexOfMax+1);
    while(cycle-- )
    {
        if(iq[i] == max)
        {
            ans[i] = 0;
            s.push(make_pair(i, max));
            
        }
        else{
            while( iq[i] >= s.top().second){
                s.pop();
            }
            ans[i] = s.top().first;
            s.push(make_pair(i, iq[i]));
        }
        if(cycle != 0 && i == persons){
            i = 1;
        }
        else if(i != persons)
            ++i;
    }
    for(int i = 1; i != ans.size(); ++i){
        printf("%d ",ans[i]);
    }
    printf("\n");
    */


    
}
